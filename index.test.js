const fs = require('fs');''
const unified = require('unified')
const markdown = require('remark-parse')
const remark2rehype = require('remark-rehype')
const html = require('rehype-stringify')
const lang = require('.')

var doc = fs.readFileSync('test.md')

unified()
    .use(markdown)
    .use(lang)
    .use(remark2rehype)
    .use(html)
    .process(doc)
    .then(
        function(file) {
            console.log(String(file))
        },
        function(err) {
            console.error(String(err))
        }
    )
