# remark-lang

This [remark](https://github.com/remarkjs/remark) plugin parses custom Markdown syntax to produce a span element with the [lang](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/lang) attribute.

## Syntax

Wrap the word or phrase with pipe characters followed by a 2 or 3 character language subtag.

Example

    ...while I ordered |김밥|ko.

This would compile to the following HTML:

    <p>...while I ordered <span lang="ko">김밥</span>.</p>

## Installation

npm:

    npm install remark-lang

## Usage

Dependencies:
    const unified = require('unified')
    const remarkParse = require('remark-parse')
    const remarkLang = require('remark-lang')
    const remark2rehype = require('remark-rehype')
    const stringify = require('rehype-stringify')

Usage:
    unified()
        .use(remarkParse)
        .use(remarkLang)
        .use(remark2rehype)
        .use(stringify)