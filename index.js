module.exports = lang

function locateLang(value, fromIndex) {
    return value.indexOf('|', fromIndex)
}

function tokenizeLang(eat, value, silent) {
    var match = /\|(.*?)\|([a-z]{2,3})/.exec(value)

    if (match) {
        if (silent) {
            return true
        }

        if (value.charAt(0) !== '|') {
            return
        }

        return eat(match[0])({
            type: 'lang',
            data: {
                hName: 'span',
                hChildren: [
                    {type: 'text', value: match[1]}
                ],
                hProperties: {
                    lang: match[2]
                }
            }
        })
    }
}

tokenizeLang.locator = locateLang

function lang() {
    var Parser = this.Parser
    var tokenizers = Parser.prototype.inlineTokenizers
    var methods = Parser.prototype.inlineMethods

    tokenizers.lang = tokenizeLang
    methods.splice(methods.indexOf('text'), 0, 'lang')
}